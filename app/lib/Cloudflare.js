'use strict'

const https = require('https')
const ShortBus = require('shortbus')
const EventEmitter = require('events').EventEmitter

class CloudFlare extends EventEmitter {
  constructor (email, secret, domain, rootdomain, callback) {
    super()

    const CFClient = require('cloudflare')

    Object.defineProperties(this, {
      domain: {
        enumerable: true,
        writable: false,
        configurable: false,
        value: domain
      },

      rootdomain: {
        enumerable: true,
        writable: false,
        configurable: false,
        value: rootdomain
      },

      email: {
        enumerable: false,
        writable: false,
        configurable: false,
        value: email
      },

      secret: {
        enumerable: false,
        writable: false,
        configurable: false,
        value: secret
      },

      domainIdentifier: {
        enumerable: false,
        writable: true,
        configurable: false,
        value: null
      },

      client: {
        enumerable: false,
        writable: false,
        configurable: false,
        value: new CFClient({
          email: email,
          key: secret
        })
      }
    })

    const me = this
    this.client.browseZones({
      name: this.rootdomain
    }).then(function (data) {
      me.domainIdentifier = data.result[0].id
      callback()
    })
  }

  purge (url, callback) {
    url = typeof url === 'string' ? [url] : url

    const iterations = Math.ceil(url.length / 30)
    const tasks = new ShortBus()

    let start = 0
    let me = this

    for (let x = 0; x < iterations; x++) {
      let urls = url.slice(start, start + 29)
      tasks.add('Purging block ' + (x + 1) + ' of ' + iterations + ' CDN cache', function (cont) {
        me.purgeFiles(urls, cont)
      })

      start += 30
    }

    tasks.on('stepcomplete', (task) => {
      console.log('  ', task.name)
    })

    tasks.on('complete', callback)

    tasks.process(true)
  }

  purgeFiles (urls, callback) {
    this.client.deleteCache(this.domainIdentifier, {
      files: urls
    }).then(callback)
  }
}

module.exports = CloudFlare
