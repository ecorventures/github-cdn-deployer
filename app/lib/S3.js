'use strict'

const AWS = require('aws-sdk')
const S3 = new AWS.S3()
const fs = require('fs')
const path = require('path')
const semver = require('semver')
const mime = require('mime')
const ShortBus = require('shortbus')
const EventEmitter = require('events').EventEmitter

class AWSS3 extends EventEmitter {
  constructor (config) {
    config = config || {}
    super()

    this.bucket = config.bucket
    this._base = config.base
  }

  get base () {
    return this._base || ''
  }

  // Upload a directory to the S3 bucket.
  upload (directory, callback) {
    const tasks = new ShortBus()
    const me = this
    const dir = fs.readdirSync(directory)

    if (dir.length > 0) {
      this.emit('highlight', 'Uploading to S3 Bucket: ' + path.join(me.bucket, this.base))
      dir.forEach((filename) => {
        tasks.add('Uploading ' + filename, function (next) {
          const mimetype = mime.lookup(path.join(directory, filename))

          S3.upload({
            Bucket: me.bucket,
            Key: path.join(me.base, filename),
            Body: fs.createReadStream(path.join(directory, filename)),
            ContentType: mimetype,
            ACL: 'public-read',
            StorageClass: 'REDUCED_REDUNDANCY'
          }, function (err, data) {
            if (err) {
              me.emit('failure', err)
            }

            next()
          })
        })
      })
    } else {
      me.emit('failure', 'No files to upload.')
      return callback && callback()
    }

    tasks.on('stepcomplete', (task) => {
      me.emit('status', task.name)
    })

    tasks.on('complete', callback)

    tasks.process(true)
  }

  getLatestFilelist (callback) {
    S3.listObjects({
      Bucket: this.bucket,
      Prefix: path.join(this.base, '..', 'latest'),
      MaxKeys: 1000
    }, (err, data) => {
      if (err) {
        me.emit('failure', err)
        return callback(null)
      }

      callback(data.Contents.map(item => {
        return item.Key
      }))
    })
  }

  // Retrieve the latest semver-compatible version number within the bucket.
  getLatestVersion (callback) {
    const me = this

    S3.listObjects({
      Bucket: this.bucket,
      Prefix: (path.join(this.base, '..') + '/').replace(/\/{2,50}/gi, '/'),
      MaxKeys: 1000
    }, (err, data) => {
      if (err) {
        me.emit('failure', err)
        return callback(null)
      }

      let content = data.Contents.map((item) => {
        let filepath = item.Key.split('/')
        return filepath[filepath.length - 2]
      }).filter((item, i, array) => {
        return array.indexOf(item) === i && semver.valid(item)
      }).sort((a, b) => {
        return semver.gt(a, b) ? -1 : 1
      })

      if (content.length === 0) {
        me.emit('failure', 'No versions found.')
        return callback(null)
      }

      callback(content[0])
    })
  }

  setLatest (version, callback) {
    const me = this
console.log(path.join(this.base, '..', version))

    S3.listObjects({
      Bucket: this.bucket,
      Prefix: path.join(this.base, '..', version),
      MaxKeys: 1000
    }, (listErr, listData) => {
      if (listErr) {
        console.log('>>>>', listErr)
        me.emit('failure', listErr)
        return callback()
      }

      if (listData.Contents.length === 0) {
        me.emit('failure', 'No files found in source bucket (' + path.join(this.base, '..', version) + ').')
        return callback()
      }

      const tasks = new ShortBus()

      tasks.on('complete', callback)

      tasks.on('stepcomplete', (task) => {
        this.emit('status', task.name)
      })

      this.clearDirectory(path.join(this.base, '..', 'latest'), () => {
        listData.Contents.forEach((item) => {
          tasks.add('Copy ' + item.Key + ' to \"latest\"', (next) => {
            let newfile = item.Key.split('/')
            newfile.shift()
            newfile.shift()

            // Support nested directories
            for (let i = 2; i < this.base.split('/').length; i++) {
              newfile.shift()
            }

            newfile = path.join(this.base, '..', 'latest', newfile.join('/'))

            S3.copyObject({
              Bucket: this.bucket,
              CopySource: path.join(this.bucket, item.Key),
              Key: newfile,
              ACL: 'public-read'
            }, (err, data) => {
              if (err) {
                this.emit('failure', err)
              }

              next()
            })
          })
        })

        tasks.process()
      })
    })
  }

  clearDirectory (dir, callback) {
    S3.listObjects({
      Bucket: this.bucket,
      Prefix: dir,
      MaxKeys: 1000
    }, (listErr, listData) => {
      if (listErr) {
        me.emit('failure', listErr)
        return callback()
      }

      const keys = listData.Contents.map((item) => {
        return {
          Key: item.Key
        }
      })

      S3.deleteObjects({
        Bucket: this.bucket,
        Delete: {
          Objects: keys
        }
      }, (err, data) => {
        if (err) {
          this.emit('failure', err)
        }

        callback()
      })
    })
  }
}

module.exports = AWSS3
