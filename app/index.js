'use strict'

require('localenvironment')
const UTIL = require('./lib/utility')
const GithubRelease = require('./lib/Release')
const GithubRepo = require('./lib/Repository')
const CloudFlare = require('./lib/Cloudflare')
const S3 = require('./lib/S3')
const ShortBus = require('shortbus')
const MustHave = require('musthave')
const chalk = require('chalk')
const path = require('path')
const os = require('os')
const fs = require('fs')
const output = path.join(os.tmpdir(), 'output')

const mh = new MustHave({
  throwOnError: false
})

// Make sure all of the configuration options are available.
if (!mh.hasAll(process.env,
  'github_owner',
  'github_repository',
  'github_token',
  'AWS_ACCESS_KEY_ID',
  'AWS_SECRET_ACCESS_KEY',
  'AWS_BUCKET')
) {
  console.log('Missing', mh.missing.join(', '))
  process.exit(1)
}

let tasks = new ShortBus()
let Release
let AWS
let CF

// Clean the working directory
tasks.add('Clean working directory', function (next) {
  UTIL.start()
  UTIL.clean(output)
  next()
})

// Setup the build
tasks.add('Setup sources and destinations', function (next) {
  Release = new GithubRelease(process.env.github_owner + '/' + process.env.github_repository, process.env.github_token)

  // Output status messages from the release
  Release.on('status', function (msg) {
    console.log(chalk.bgBlack.gray('   [' + Release.repository.toUpperCase() + ' GITHUB RELEASE] ' + msg))
  })

  if (mh.hasAny(process.env, 'CLOUDFLARE_API_SECRET')) {
    CF = new CloudFlare(
      process.env.CLOUDFLARE_EMAIL,
      process.env.CLOUDFLARE_API_SECRET,
      process.env.AWS_BUCKET,
      process.env.CLOUDFLARE_DOMAIN, function () {
        console.log(chalk.bgBlack.gray('   CLOUDFLARE SETUP COMPLETE'))
        next()
      }
    )

    CF.on('status', (status) => {
      console.log('  ', chalk.gray(status))
    })
  } else {
    next()
  }
})

// Pull from Github Release
tasks.add('Pull from Github', function (next) {
  Release.downloadLatest(output, next)
})

// Push to S3
tasks.add('Push to Amazon S3', function (next) {
  AWS = new S3({
    bucket: process.env.AWS_BUCKET,
    base: path.join(process.env.AWS_ROOTPATH, Release.version)
  })

  AWS.on('failure', (err) => {
    console.log('  ', chalk.bgRed.bold.white('  AWS ERROR:', err.message, '  '))
  })

  AWS.on('status', (status) => {
    console.log('  ', chalk.gray(status))
  })

  AWS.on('highlight', (status) => {
    console.log('  ', chalk.yellow(status))
  })

  AWS.upload(output, () => {
    console.log('\n  ', chalk.bgGreen.black(' Published v' + Release.version, ' '), '\n')
    next()
  })
})

let cachelist = []
if (mh.hasAny(process.env, 'CLOUDFLARE_API_SECRET')) {
  tasks.add('Acquire cache list for \"latest\" version', function (next) {
    AWS.getLatestFilelist(function (list) {
      list = list.map(uri => {
        return 'http://' + process.env.AWS_BUCKET + '/' + uri
      })

      let ssllist = list

      cachelist = list.concat(ssllist.map(uri => {
        return uri.replace('http://', 'https://')
      }))

      next()
    })
  })
}

tasks.add('Update \"latest\" version', function (next) {
  // AWS.getLatestVersion(function (version) {
    // version = version ||
    console.log(Release.version)
    AWS.setLatest(Release.version, next)
  // })
})

if (mh.hasAny(process.env, 'CLOUDFLARE_API_SECRET')) {
  tasks.add('Purge cache of old assets', function (next) {
    CF.purge(cachelist, next)
  })
}

// tasks.add('Cleanup', function (next) {
//   UTIL.deleteDirectory(output)
//   next()
// })

tasks.on('stepstarted', function (task) {
  console.log('\n', chalk.underline.green('Step', task.number + ')', task.name))
})

tasks.on('stepcomplete', (task) => {
  console.log(chalk.bgBlack.white('  ', chalk.yellow.bold(task.name), 'completed in ') + chalk.bgBlack.bold.yellow(UTIL.duration) + chalk.bgBlack.white(' seconds.'))
})

// Log completion. Trigger any folowup tasks.
tasks.on('complete', () => {
  console.log('\n\n', chalk.bgMagenta.black('  Process Complete  '), '\n')
})

tasks.process(true)
