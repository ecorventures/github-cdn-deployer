FROM mhart/alpine-node:6.2.2

WORKDIR /app
ADD ./app /app

RUN apk --update add git && npm i

CMD ["npm", "start"]
